function todo
	todotxt-machine ~/todo.txt/todo.txt ~/todo.txt/done.txt
end

function vagrant-up 
    sudo modprobe vboxdrv;
    sudo modprobe vboxnetflt;
    sudo modprobe vboxnetadp;
    cd ~/vagrant-local;
    vagrant up;
end
